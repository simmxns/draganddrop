# Drag & Drop ✊
Simple drag and drop with Sorteable.js

## Demo
<div align="center">
  <img src="" />

</div>

<p><em>30fps btw :c</em></p>

## Tech
* HTML, CSS, JS
* [Box icons](https://boxicons.com/)
* [Sorteable.js](https://github.com/SortableJS/Sortable)
