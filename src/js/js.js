const dropItem = document.getElementById('drop-items')

new Sortable(dropItem, {
	swap: true,
	swapClass: 'highlight-drop',
	animation: 350,
})
